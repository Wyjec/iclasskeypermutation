import sys

if len(sys.argv) != 2:
  print "Usage: permuteKey.py <key>"
  sys.exit() 

key = sys.argv[1]

c=2
key = [key[i:i+c] for i in range(0,len(key),c)]

shift = 0

permutedKey = ""

while shift < 8:
  mask = 1 << shift
  byte = ""
  for s in key:
    byte += str((int(s,16) & mask) >> shift)

  permutedKey += hex(int(byte,2))[2:]
  shift += 1

print permutedKey.upper()